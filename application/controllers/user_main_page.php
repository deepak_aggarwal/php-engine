<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user_main_page
 *
 * @author Deepak
 */
class user_main_page extends CI_Controller{
    public function __construct() {
		session_start();
        parent::__construct();
        if(!isset($_SESSION['username']))
        {  
            redirect('login');
        }
    }
    public function index()
    {
        $this->load->view('user_main_page');
    }
    //put your code here
}

?>
