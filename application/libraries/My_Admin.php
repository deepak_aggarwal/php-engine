<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author Deepak
 */
class My_Query {
    //put your code here
    
    
    
    
    const delete_task =" DELETE FROM task_assign_to WHERE task_id=6;
                         DELETE FROM tasks where task_id=6";
}

class My_Admin {
    
    private $ci_instance;
    private $account_number;
    function __construct($param)
    {
        $this->ci_instance=& get_instance();
        $this->account_number=$param['account_number'];
    }
    function get_users()
    {    
        $query = $this->ci_instance->db->query("SELECT users_credentials.firstname, users_credentials.lastname, 
                     users_credentials.email  FROM users_credentials inner join role_assign_user
                      ON users_credentials.user_id = role_assign_user.user_id INNER JOIN roles
                     ON role_assign_user.role_id=roles.role_id where users_credentials.account_id=?",
                array($this->account_number));
        return $query;
    }
    function add_user($dtls)
    {
        $this->ci_instance->db->trans_begin();
        $this->ci_instance->db->query("INSERT INTO users_credentials (username, password, email, firstname, lastname, account_id ) 
                    values( ?,?,?,?,?,?)",
                  array( $dtls['username'],$dtls['password'],$dtls['email'],
                      $dtls['firstname'],$dtls['lastname'],  $this->account_number));
        $id= $this->ci_instance->db->insert_id();
        $this->ci_instance->db->query("INSERT INTO users_details (user_id,gender, telephone,mobile_number,address,zipcode,country)
                                      values($id,' ',' ', ' ', ' ', ' ', ' ')");
        $this->ci_instance->db->query("INSERT INTO users_maintainance (user_id, activation_key, password_reset_key)
                    values ($id,NULL,NULL)");
        $this->ci_instance->db->query("INSERT INTO role_assign_user(role_id,user_id)
                    values(?,$id)",array($dtls['role_id']));
        if($this->ci_instance->db->trans_status()==FALSE)
        {
            $this->ci_instance->db->trans_rollback();
        }
        else
        {
            $this->ci_instance->db->trans_commit();
        }
    }
    function edit_user()
    {
        
    }
    function delete_user($user_id)
    {
        $this->ci_instance->db->trans_begin();
        $query = $this->ci_instance->db->query("DELETE FROM users_credentials
                          where user_id= ?",array($user_id));
        if($this->ci_instance->db->trans_status()== FALSE)
        {
            $this->ci_instance->db->trans_rollback();
            return false;
        }
        else
        {
            $this->ci_instance->db->trans_commit();
            return false;
        }
    }
    function add_role($details)
    {
        $query = $this->ci_instance->db->query("INSERT INTO roles(role_name,user_type,account_id)
                    values(?,?,?)",array($details['role_name'],$details['user_type'],  $this->account_number));
        if($this->ci_instance->db->affected_rows()>=0) 
            return true;
        else
            return false;
        
    }
    function delete_role($role_id)
    {
        $query = $this->ci_instance->db->query("DELETE FROM roles WHERE role_id=?",array($role_id));
        if($this->ci_instance->db->affected_rows()>=0) 
            return true;
        else
            return false;
    }
    function edit_role()
    {
        
    }
    
    function create_new_task($task)
    {
        $this->ci_instance->db->trans_begin();
        $this->ci_instance->db->query("INSERT INTO tasks(start,deadline,title,description,to_be_approved_by,status,account_id)
                            values(?,?,?,?,?,?,?)",
                array($task['start'],$task['deadline'],$task['title'],$task['description'],
                    $task['to_be_approved_by'],$task['status'],  $this->account_number));
        $new_task_id=$this->ci_instance->db->insert_id();
        $this->ci_instance->db->query("INSERT INTO task_assign_to(task_id,user_id)
                            values(?,?)",array($new_task_id,$task['user_id']));
        if($this->ci_instance->db->trans_status()==FALSE)
        {
            $this->ci_instance->db->trans_rollback();
            return false;
        }
        else
        {
            $this->ci_instance->db->trans_commit();
            return true;
        }
    }
    function delete_task($task_id)
    {
        $this->ci_instance->db->trans_begin();
        $this->ci_instance->db->query("DELETE FROM tasks where task_id=?",array($task_id));
        if($this->ci_instance->db->trans_status()==False)
        {
            $this->ci_instance->db->trans_rollback();
            return false;
        }
        else
        {
            $this->ci_instance->db->trans_commit();
            return true;
        }
    }
    function edit_task()
    {
        
    }
    function approve_task()
    {
        
    }
}
?>
 