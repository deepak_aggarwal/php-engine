<div class="row-fluid well">
    <div class="span10">
        <?php $attributes = array('class' => 'form-horizontal','id' => 'add_member_form');
        echo form_open('admin/add_member/', $attributes); ?>
        <fieldset>
            <h3>Add New Member</h3>
            <hr>
            <div class="control-group">
                <label class="control-label" for="member_name">Name</label>
                <div class="controls">
                    <input class="input-xlarge focused" id="member_name" type="text" placeholder="e.g. John Smith" name="member_name">
                </div>
            </div> 
            <div class="control-group">
                <label class="control-label" for="member_email">E-mail</label>
                <div class="controls">
                    <input class="input-xlarge focused" id="member_email" type="text" placeholder="name@domain.com" name="member_email">

                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="member_number">Phone</label>
                <div class="controls">
                    <input class="input-xlarge focused" id="member_number" type="text" name="member_number" >
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="optionsCheckbox">Select permissions</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox">
                        Publish and edit posts </label>
                    <label class="checkbox">
                        <input type="checkbox">
                        Read and generate reports </label>
                    <label class="checkbox">
                        <input type="checkbox">
                        Assign tasks </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="optionsCheckbox">Select profiles</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="checkbox">
                        Facebook </label>
                    <label class="checkbox">
                        <input type="checkbox">
                        Twitter </label>
                    <label class="checkbox">
                        <input type="checkbox">
                        Blog </label>
                </div>
            </div>
            <div class="span4"><input type="submit" class="btn btn-primary pull-right" value="Add"></div>
        </fieldset>
        </form>
    </div>
</div>