<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login_check
 *
 * @author Deepak
 */
class login_check  extends CI_Model{
    
    public function auth_user($username, $password)
    {
        $sql = "Select user_id from users_credentials where username=? and password = ? limit 1";
        $result= $this->db->query($sql,array($username,$password));
        if($result->num_rows() > 0)
        {
            $sql="Select user_type,role_name,user_id,account_id from roles inner join role_assign_user on 
                  role_assign_user.role_id = roles.role_id where user_id = ?";
            $user_result = $this->db->query($sql,array($result->row()->user_id));
            return $user_result->row();
        }
        else
        {
            return false;
        }
    }
}

?>
