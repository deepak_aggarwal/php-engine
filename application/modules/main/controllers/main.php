<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of main
 *
 * @author Deepak
 */
class main extends MX_Controller {

    function __construct() {
        parent::__construct();
        require_once 'Factory.php';
    }

    public function index() {
        $this->_display_main_page();
    }

    private function _display_main_page() {
        $this->load->view("main_page");
    }

    //put your code here
}

?>
