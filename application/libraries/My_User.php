<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_User
 *
 * @author Deepak
 */
class My_Query {
    //put your code here
    const get_user_detail ="SELECT users_details.*,roles.role_name,roles.user_type
        from users_details inner join role_assign_user on 
        users_details.user_id=role_assign_user.user_id 
        inner join roles on role_assign_user.role_id = roles.role_id
        where users_details.user_id =? limit 1";
}
class My_User{
    private $user_id;            //used for storing user id 
    private $ci_instance;        //used for storing instance of codeigniter
    public  $user_detail;
    private $user_type;
    
    // constructor intializing 
    function __construct ($param)               // set user id and intitalize its all details 
    {
       $this->ci_instance=& get_instance();
       $this->user_id=$param['user_id'];
       $result= $this->ci_instance->db->query(My_Query::get_user_detail,array($this->user_id));
       if($result->num_rows()>0)
       {
           $this->user_detail= $result->row();
           return true;
       }
       else
           return false; 
       $this->user_type=$this->user_detail->user_type;
    }
    function get_user_type()
    {
        echo $this->user_detail->user_type;
        
    }   
    function get_my_task()
    {
        $result=$this->ci_instance->db->query("Select start,deadline,title,description from tasks where status='active' 
                   and task_id IN(Select task_id from task_assign_to where user_id=?)",array($this->user_id));
        if($result->num_rows >0) // User have a active task 
        {
            return $result;
        }
        else
            return false; // i.e User doesn't have any active task  
    }
}

?>
