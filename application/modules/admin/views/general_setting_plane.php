<div class="row-fluid">
    <div class="span12">
        <form class="form-horizontal">
            <fieldset>
                <legend>General Settings</legend>
                <br>
                <h4>Edit Profiles</h4>
                <br>
                <ul class="nav nav-tabs">
                    <li class="active"> <a href="#">Twitter</a> </li>
                    <li> <a href="#">Facebook</a> </li>
                    <li> <a href="#">RSS/Atom feed</a> </li>
                </ul>
                <div class="span12">
                    <form class="form-inline">
                        <input type="text" class="input-xlarge focused" placeholder="bill@microsoft.com">
                        <input type="password" class="input-xlarge focused" placeholder="***************">
                        <button type="submit" class="btn btn-danger btn-mini pull-right">Remove</button>
                    </form>
                    <br>
                    <br>
                    <form class="form-inline">
                        <input type="text" class="input-xlarge focused" placeholder="E-mail">
                        <input type="password" class="input-xlarge focused" placeholder="Password">
                        <button type="submit" class="btn btn-success btn-mini pull-right">Add Profile</button>
                    </form>
                    <form class="form-inline">
                        <input type="text" class="input-xlarge focused" placeholder="E-mail">
                        <input type="password" class="input-xlarge focused" placeholder="Password">
                        <button type="submit" class="btn btn-success btn-mini pull-right">Add Profile</button>
                    </form>
                    <br>
                    <br>
                    <div class="span4"> <a class="btn btn-primary" href="#" id="add_profile"> <i class="icon-plus-sign icon-white"></i> Add More Profiles </a> </div>
                    <br>
                </div>
            </feildset>
        </form>
    </div>
</div>