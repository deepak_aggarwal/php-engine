<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'phpass-0.3/PasswordHash.php';

class login extends MX_Controller {

    function __construct() {
        if (!$this->input->is_ajax_request())
            redirect("404");
        session_start();
        parent::__construct();
    }

    public function index() {
        $this->load->library('form_validation');
        // Username is required
        $this->form_validation->set_rules('username', 'Username ', 'required');
        // Minimum length of password should be 4
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');


        if ($this->form_validation->run() != false) {
            // forms validates
            $this->load->model('login_check');
            $result = $this->login_check->auth_user(
                    $this->input->post('username'), $this->input->post('password'));
            if ($result != false) {
                $_SESSION['user_type'] = $result->user_type;
                $_SESSION['user_id'] = $result->user_id;
                $_SESSION['role_name'] = $result->role_name;
                $_SESSION['logged_in'] = true;
                $_SESSION['account_id']=$result->account_id;
                if ($result->user_type == 'admin') {
                    $data = array('login' => "true", 'user_type' => "admin");
                } 
                else if ($result->user_type == 'user') {
                    $data = array('login' => "true", 'user_type' => "user");
                }
                $data['username']= $this->input->post('username');
                echo json_encode($data);
            }
            else
                $this->_show_login_form();
        }
        else
            $this->_show_login_form();
    }

    public function _show_login_form() {
        $this->load->view('login');
    }

    public function logout() {
        session_destroy();
        $this->load->view('login');
    }

}

?>
