<div class="container" id="content_plane" style="padding-top:80px">
    <div class="row-fluid">
        <div  class="offset4" id="setting_buttons">
            <div class="btn-group">
                <a class="btn active" id="general_settings">General Settings</a>
                <a class="btn" id="team_settings">Team Settings</a>
            </div>
        </div>
        <hr></hr>
    </div>
    <div id="setting_content" class="row-fluid">
        
    </div>
</div>