<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'phpass-0.3/PasswordHash.php';
class login extends CI_Controller
{
    function __construct()
    {
	session_start();
        parent::__construct();
    }
    public function index()
    {
        $this->load->library('form_validation');
        // Username is required
        $this->form_validation->set_rules('username','Username ','required');
        // Minimum length of password should ne 8
        $this->form_validation->set_rules('password','Password','required|min_length[4]');
        
        
        if($this->form_validation->run()!=false)
        {
            // forms validates
            $this->load->model('login_check');
            $result = $this->login_check->auth_user(
                    $this->input->post('username'),
                    $this->input->post('password'));
            if($result != false)
            {
                 $_SESSION['user_type']=$result->user_type;
                 $_SESSION['user_id']=$result->user_id;
                 $_SESSION['role_name']=$result->role_name;
                 redirect('main');
            }
        }
        $this->load->view('login');
    }
    public function logout()
    {
        session_destroy();
        $this->load->view('login');
    }
    
}

?>
