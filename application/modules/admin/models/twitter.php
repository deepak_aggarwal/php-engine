<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of twitter
 *
 * @author Deepak
 */
class twitter extends CI_Model{
    //put your code here
    private function check_user($id)
    {
        $sql="Select twitter_id from twitter_credentials where twitter_id =?";
        $result=$this->db->query($sql,array($id));
        if($result->num_rows()>0)
        {
            return false;
        }
        else
            return true;
    }
    public function add_user($param)
    {
       if(!$this->check_user($param['twitter_id']))   // If it return false then we should return false 
           return false;
       else {
           $sql=" Insert into twitter_credentials set
                account_account_id =?, oauth_token=?, oauth_token_secret=?,
                twitter_id=?";
           $result= $this->db->query($sql,array($param['account_id'],$param['oauth_token'],$param['oauth_token_secret'],$param['twitter_id']));
           return $result;
       }
    }
}

?>
