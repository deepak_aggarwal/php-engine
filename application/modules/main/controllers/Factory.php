
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Factory
 *
 * @author Deepak
 */
class Factory {
    public static function get_user_instance($user_type,$user_id)
    {
        $param=array('user_id'=>$user_id);
        if($user_type=='user')
        {
            $this->load->library('MY_User',$param);
            return $this->my_user;
        }
        else
        {
            if($user_type=='admin')
            {
                $this->load->library('My_Admin',$param);
                return $this->my_admin;
            }
            else
                return null;
        }
    }
}

?>
