<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author Deepak
 */
class admin extends MX_Controller {

    public function __construct() {
        session_start();
        if (!(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 'admin')) {  // If user is not a admin
            redirect("404");
        }
        parent::__construct();
    }

    public function index() {
        $this->load->view("content_plane");
    }

    public function settings($param) {
        if ($param == "get_content_plane") {
            $this->load->view("get_settings_content_plane");
        }
    }

    public function get_user() {
        echo '<h4>Existing Members</h4>
					<br>
					<!-- this is where i have to make changes add node and validate-->
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>E-mail</th>
								<th>Permissions</th>
								<th>Profiles</th>
							</tr>
						</thead>
						<tbody id="tbody">
						</tbody>
					</table>
         <br><br>   
        <button type="submit" class="btn btn-primary pull-right" id="add_member"><i class="icon-plus-sign icon-white"></i> Add Member</button>';
    }

    public function load_add_member_form() {
        $this->load->view("add_member_form");
    }

    public function add_member() {  // Responsible for adding member temp before actual sign up(invitation)
    // TODO
        $param = array();
        $param['member_name'] = $this->input->post('member_name');
        $param['member_email'] = $this->input->post('member_email');
        $param['member_number'] = $this->input->post('member_number');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('member_name', 'Name', 'required|alpha');
        $this->form_validation->set_rules('member_email', 'E-mail', 'required|valid_email');
        $this->form_validation->set_rules('member_number', 'Phone', 'required');
        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
            return;
        }
        $this->load->model('admin_model');
        $result = $this->admin_model->check_for_duplicate_user($param['member_email']);
        if ($result == true) {
            
        }
    }

    public function get_general_setting_plane() {
        $this->load->view('general_setting_plane');
    }

    public function add_twitter_account() {

        // It really is best to auto-load this library!
        //$this->load->library('tweet');

        // Enabling debug will show you any errors in the calls you're making, e.g:
        $this->tweet->enable_debug(TRUE);

        // If you already have a token saved for your user
        // (In a db for example) - See line #37
        // 
        // You can set these tokens before calling logged_in to try using the existing tokens.
        // $tokens = array('oauth_token' => 'foo', 'oauth_token_secret' => 'bar');
        // $this->tweet->set_tokens($tokens);

        
            // This is where the url will go to after auth.
            // ( Callback url )

            $this->tweet->set_callback(site_url('admin/twitter_auth'));

            // Send the user off for login!

            $this->tweet->login();
    }
    function twitter_auth() {
        if (!$this->tweet->logged_in())   // If a user is not logged in then this function shouldn't run
            return ;
        $tokens = $this->tweet->get_tokens();
        // $user = $this->tweet->call('get', 'account/verify_credentiaaaaaaaaals');
        // 
        // Will throw an error with a stacktrace.

        $user = $this->tweet->call('get', 'account/verify_credentials');
        $this->load->model("twitter");
        $param= array();
        $param['account_id']=$_SESSION['account_id'];
        $param['oauth_token']=$tokens['oauth_token'];
        $param['oauth_token_secret']=$tokens['oauth_token_secret'];
        $param['twitter_id']=$user->id;
        
        if($this->twitter->add_user($param))
            $message['message']= "Account added successfully";
        else
            $message['message']=" There is some problem while adding your account";
        
        $this->tweet->logout();  //Useful if a user add mutiple account at same instance
        $this->load->view("close_twitter_popup",$message);
        
        /*
        $friendship = $this->tweet->call('get', 'friendships/show', array('source_screen_name' => $user->screen_name, 'target_screen_name' => 'elliothaughin'));
        var_dump($friendship);
        
        if ($friendship->relationship->target->following === FALSE) {
            $this->tweet->call('post', 'friendships/create', array('screen_name' => $user->screen_name, 'follow' => TRUE));
        }

        $this->tweet->call('post', 'statuses/update', array('status' => 'Testing #CodeIgniter Twitter library by @elliothaughin - http://bit.ly/grHmua'));

        $options = array(
            'count' => 10,
            'page' => 2,
            'include_entities' => 1
        );

        $timeline = $this->tweet->call('get', 'statuses/home_timeline');

        var_dump($timeline);
       * */
    }

}

?>
