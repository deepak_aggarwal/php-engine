<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of test
 *
 * @author Deepak
 */
require_once APPPATH.'libraries/REST_Controller.php';
class rest extends REST_Controller{
    //put your code here
    public function __construct()
    {
        parent::__construct();
         session_start();
       if(!(isset ($_SESSION['user_type'])&&$_SESSION['user_type']== 'admin'))  // If user is not a admin
        {
            $this->response(NULL,404);
        }
    }
     public function __autoload()
    {
        
    }
    public function main_get()
    {
        $to_be_send= array();
        array_push($to_be_send,array(
                        'id'=> 'type/user',
                        'firstName'=>"rohan", 'lastName'=>"gulati", 
			'email'=>"rohangulati@gmail.com", 
			'phone'=>"9899331257", 
			'permission'=>"all", 
			'profile'=>"all"));
        array_push($to_be_send,array(
                        'id'=> 'type/user',
                        'firstName'=>"raman", 'lastName'=>"gupta", 
			'email'=>"ramangupta@gmail.com", 
			'phone'=>"9878430222", 
			'permission'=>"all", 
			'profile'=>"all"));
        array_push($to_be_send,array(
                        'id'=> 'type/user',
                        'firstName'=>"nehal", 'lastName'=>"gupta", 
			'email'=>"nehalgupta@gmail.com", 
			'phone'=>"9878430222", 
			'permission'=>"all", 
			'profile'=>"all"));
        
        switch ($this->get("type"))
        {
            case "user":  $this->response(json_encode($to_be_send),200);
        }
    }
    public function user_get()
    {
        $this->response(json_encode(array('user'=>'deepak')),200);
    }
    public function user_put()
    {
        //$this->response(json_encode(array('user'=>'deepak')),200);
    }
    public function user_delete()
    {
        echo "hi";
    }
    public function user_post()
    {
        echo "hi";
    }
    public function profile_get()
    {
        $result=$this->db->query("Select * from twitter_credentials where account_account_id=?",$_SESSION['account_id']);
        if($result->num_rows>0)
        {
           $json= array ();
            foreach ($result->result() as $res)
            {
                $json[]=array('twitter_account_id'=>$res->twitter_account_id,'tag'=>$res->tag);
            }
        }
        
        $this->response(json_encode($json),200);
    }
    public function profile_delete()
    {
        echo "hi";
    }
    public function profile_post()
    {
        $this->response(json_encode(array('user'=>'deepak')),200);
    }
    
}

?>
